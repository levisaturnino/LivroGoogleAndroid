package com.example.saturnino.livroandroidcap4_activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by saturnino on 06/04/15.
 */
public class ExemploClicoVidaAbrirTela extends ExemploCicloVida implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Button b = new Button(this);
        b.setText("Clique aqui para abrir a tela");
        b.setOnClickListener(this);
        setContentView(b);
    }

    @Override
    public void onClick(View v) {

        Intent it = new Intent(this,Tela2.class);

      //  Bundle  params = new Bundle();
      //  params.putString("mgs","Olá");
     //   it.putExtras(params);
        it.putExtra("msg","olá");
         startActivity(it);
    }
}
