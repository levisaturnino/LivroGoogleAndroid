package com.example.saturnino.livroandroidcap4_activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by saturnino on 06/04/15.
 */
public class ExemploSmileAdapter extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<Smile> list = new ArrayList<>();
        list.add(new Smile("Feliz",Smile.FELIZ));
        list.add(new Smile("Triste",Smile.TRISTE));
        list.add(new Smile("Louco",Smile.LOUCO));
        // Layout customizado para cada linha do smile
        setListAdapter(new SmileAdapter(this,list));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Smile smile = (Smile) this.getListAdapter().getItem(position);

        Toast.makeText(this,"Você selecinou o smile: "+smile.nome,Toast.LENGTH_SHORT).show();
    }
}
