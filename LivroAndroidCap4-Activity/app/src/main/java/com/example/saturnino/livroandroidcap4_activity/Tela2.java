package com.example.saturnino.livroandroidcap4_activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by saturnino on 06/04/15.
 */
public class Tela2  extends ExemploCicloVida{

    protected static final String CATEGORIA = "livro";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView view = new TextView(this);
        view.setText("Esta é a tela 2");
        setContentView(view);

        Intent it = getIntent();

        if(it != null)
        {

          /*Bundle params = it.getExtras();
            if(params !=null)
            {
                String msg = params.getString("mgs");
                Log.i(CATEGORIA,"Mensagem: "+msg);
            }*/

            String msg = it.getStringExtra("msg");
            Log.i(CATEGORIA,"Mensagem: "+msg);

        }

    }
}
