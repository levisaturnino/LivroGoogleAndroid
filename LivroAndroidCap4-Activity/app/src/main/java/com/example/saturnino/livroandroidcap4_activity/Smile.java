package com.example.saturnino.livroandroidcap4_activity;

/**
 * Created by saturnino on 06/04/15.
 */
public class Smile {


    public static final int FELIZ = 0;
    public static final int TRISTE = 1;
    public static final int LOUCO = 2;

    public String nome;
    private final int tipo;

    public Smile(String nome,int tipo) {
        this.tipo = tipo;
        this.nome = nome;
    }



    public int getImagem()
    {
        switch (tipo)
        {
            case FELIZ:
                return R.mipmap.ic_launcher;
            case TRISTE:
                return R.mipmap.ic_launcher;
            case LOUCO:
                return R.mipmap.ic_launcher;
        }
        return R.mipmap.ic_launcher;
    }


}
