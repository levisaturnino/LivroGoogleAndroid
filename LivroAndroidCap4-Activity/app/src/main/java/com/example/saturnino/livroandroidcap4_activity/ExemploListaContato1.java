package com.example.saturnino.livroandroidcap4_activity;

import android.app.ListActivity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by saturnino on 06/04/15.
 */
public class ExemploListaContato1 extends ListActivity {

    private ArrayAdapter<String> adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Uri para Buscar o contatos
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        // Recupera o cursor dos contatos
        Cursor c = getContentResolver().query(uri,null,null,null,null);
        // Utiliza o ArrayAdapter, para exibir o array de String na telea.
        adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        // Percorre os elementos do cursor, que são os contatos da agenda
        while(c.moveToNext())
        {
            adaptador.add(c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
        }
        // Informa o adapter responsável por exibir a lista do ListActivity
        setListAdapter(adaptador);
        //podemos fechar o cursor depois de utilizá-lo
        c.close();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // Clicou em um item da lista
        String s = (String) adaptador.getItem(position);
        Toast.makeText(this,"Contato selecionado: "+s,Toast.LENGTH_SHORT).show();
    }
}
