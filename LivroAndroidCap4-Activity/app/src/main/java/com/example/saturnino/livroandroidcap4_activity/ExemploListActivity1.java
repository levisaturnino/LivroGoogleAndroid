package com.example.saturnino.livroandroidcap4_activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by saturnino on 06/04/15.
 */
public class ExemploListActivity1 extends ListActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Array de String para visualizar na Lista
        String[] itens = new String[] {"Nome 1", "Nome 2","Nome 3"};
        // Utiliza o adaptador ArrayAdapter, para exibir o array de String na tela
        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,itens);
        setListAdapter(arrayAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // Pega o item naquela posição

        Object o = this.getListAdapter().getItem(position);
        String item = o.toString();

        Toast.makeText(this,"você selecinou: "+item,Toast.LENGTH_SHORT).show();

    }
}
