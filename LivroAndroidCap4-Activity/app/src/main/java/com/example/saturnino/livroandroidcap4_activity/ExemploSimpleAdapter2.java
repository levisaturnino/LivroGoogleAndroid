package com.example.saturnino.livroandroidcap4_activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by saturnino on 06/04/15.
 */
public class ExemploSimpleAdapter2 extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    // Apenas para brincar... sobrescvre o layout do Listview default do Android
        setContentView(R.layout.layout_listview_contatos);
        ArrayList<HashMap<String,String>> list = new ArrayList<>();

        // Cada item de uma linha precisa ser um HashMap
        // o HashMap contém as chaves e valores necessários paar preencher os elementos da tela

        for(int i = 0; i < 10 ; i++)
        {
            HashMap<String, String> item = new HashMap<>();
            item.put("nome","Nome "+i);
            item.put("fone","Fone "+i);
            list.add(item);
        }

        // Utilizar o adapter SimpleAdapter
        // Array que define as chaves do HashMap

        String[] from = new String[] {"nome","fone"};
        // text1 e text2 são padrões do android para o layout nativo "two_line_list_item"
        int[] to = new int[]{R.id.nome,R.id.fone};

        setListAdapter(new SimpleAdapter(this,list,R.layout.layout_contatos_fone,from,to));

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // Pega o item naquela posição

        Object o = this.getListAdapter().getItem(position);
        String item = o.toString();

        Toast.makeText(this, "você selecinou: " + item, Toast.LENGTH_SHORT).show();

    }
}
