package com.example.saturnino.livroandroidcap4_activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.TextView;


public class ExemploCicloVida extends ActionBarActivity {

    protected static final String CATEGORIA = "livro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(CATEGORIA, getClassName() + ".onCreate() chamado: " + savedInstanceState);
        TextView t = new TextView(this);
        t.setText("Exemplo do ciclo de vida.\nConsulte os logs no LogCat.");
        setContentView(t);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(CATEGORIA, getClassName() + ".onStart() chamado");
        }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(CATEGORIA, getClassName() + ".onRestart() chamado");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(CATEGORIA, getClassName() + ".onResume() chamado");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(CATEGORIA, getClassName() + ".onPause() chamado");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(CATEGORIA, getClassName() + ".onStop() chamado");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(CATEGORIA, getClassName() + ".onDestroy() chamado");
    }


    private String getClassName()
 {     // Returna o nome da classe sem o pacote
     String s = getClass().getName();
     return s.substring(s.lastIndexOf("."));
 }

}
