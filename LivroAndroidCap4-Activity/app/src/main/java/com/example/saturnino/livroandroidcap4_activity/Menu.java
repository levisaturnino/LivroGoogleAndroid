package com.example.saturnino.livroandroidcap4_activity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by saturnino on 06/04/15.
 */
public class Menu extends ListActivity {

    private static final String[] nomes = new String[]{
            "Ciclo de vida","ArrayAdapter","SimplesAdapter 1",
            "SimplesAdapter 2","CursorAdapter 1",
            "Customizado - Smile","Sair"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    this.setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,nomes));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position)
        {
            case 0:
                startActivity(new Intent(this,ExemploCicloVida.class));
                break;
            case 1:
                startActivity(new Intent(this,ExemploListActivity1.class));
                break;
            case 2:
                startActivity(new Intent(this,ExemploSimpleAdapter1.class));
                break;
            case 3:
                startActivity(new Intent(this,ExemploSimpleAdapter2.class));
                break;
            case 4:
                startActivity(new Intent(this,ExemploListaContato1.class));
                break;
            case 5:
                startActivity(new Intent(this,ExemploSmileAdapter.class));
                break;
            default:
                finish();
        }
    }
}
