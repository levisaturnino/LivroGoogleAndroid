package com.example.saturnino.livroandroidcap05_intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by saturnino on 08/04/15.
 */
public class ExemploContatoTodosCelular extends ActionBarActivity implements View.OnClickListener {

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_contato_todos);

final Button button = (Button) findViewById(R.id.botaoOK);
        button.setOnClickListener(this);
        }
@Override
public void onClick(View v) {

        Uri uri = Uri.parse("content://com.android.contacts/contacts/");
        // ACTION_PICK exibi todos os contatos do celular
        Intent intent = new Intent(Intent.ACTION_PICK,uri);

        startActivity(intent);

        }
}