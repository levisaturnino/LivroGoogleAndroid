package com.example.saturnino.livroandroidcap05_intent;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by saturnino on 08/04/15.
 */
public class ExemploContatoPeloNome extends ActionBarActivity implements View.OnClickListener {

    private static int SELECIONAR_CONTATO = 1;
    private static final String CATEGORIA = "livro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_contato_todos);

        final Button button = (Button) findViewById(R.id.botaoOK);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Uri uri = Uri.parse("content://com.android.contacts/contacts/");
        // ACTION_PICK exibi todos os contatos do celular
        Intent intent = new Intent(Intent.ACTION_PICK, uri);

        startActivityForResult(intent,SELECIONAR_CONTATO);

    }

    @Override
    protected void onActivityResult(int codigo, int resultado, Intent it) {
        super.onActivityResult(codigo, resultado, it);

        if(it == null)
        {
            Toast.makeText(this,"Nenhum contato",Toast.LENGTH_SHORT).show();
            return;
        }
        // URI para visualizar o contato selecionado
        Uri uri = it.getData();
        Cursor c = getContentResolver().query(uri,null,null,null,null);
        // Posiciona o cursor;
        c.moveToNext();
        // Recupra o nome do contato
        int nameColumn = c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME);
        String nome = c.getString(nameColumn);
        Toast.makeText(this,"Nome: "+nome,Toast.LENGTH_SHORT).show();
        c.close();

    }
}