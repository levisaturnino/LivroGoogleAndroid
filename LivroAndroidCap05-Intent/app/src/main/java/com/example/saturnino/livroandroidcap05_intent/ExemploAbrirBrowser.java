package com.example.saturnino.livroandroidcap05_intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class ExemploAbrirBrowser extends ActionBarActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_browser);

        final Button button = (Button) findViewById(R.id.botaoOK);
        button.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        EditText campoEndereco = (EditText) findViewById(R.id.campoEndereco);
        String endereco = campoEndereco.getText().toString();
        // Representa o endereço que desejamos abrir

        Log.i("INDEXOF",""+endereco.indexOf("http://"));

        Uri uri;
        if (endereco.indexOf("http://") >= 0){

           uri = Uri.parse(endereco);

        }
        else
        {
            uri = Uri.parse("http://"+endereco);
        }
        // Cria a Intent com o endereço
        Intent intent = new Intent(Intent.ACTION_VIEW,uri);
        // Envia a mensagem ao sistema opecional
        startActivity(intent);

    }
}
