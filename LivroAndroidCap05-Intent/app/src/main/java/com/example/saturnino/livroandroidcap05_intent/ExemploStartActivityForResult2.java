package com.example.saturnino.livroandroidcap05_intent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by saturnino on 08/04/15.
 */
public class ExemploStartActivityForResult2 extends ActionBarActivity implements View.OnClickListener {

    private static int ACTIVITY_SIM_NAO = 1;
    private static final String CATEGORIA = "livro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_return);

        final Button button = (Button) findViewById(R.id.botaoOK);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        Intent intent = new Intent(this,ExemploTelaSimNao.class);

        startActivityForResult(intent,ACTIVITY_SIM_NAO);

    }

    @Override
    protected void onActivityResult(int codigo, int resultado, Intent it) {
        super.onActivityResult(codigo, resultado, it);

  if(codigo == ACTIVITY_SIM_NAO)
  {
      Bundle params = it != null? it.getExtras():null;
    if(params != null)
    {
        String msg = params.getString("msg");
        if(resultado == 1)
        {
            // sim
            Toast.makeText(this, "Escolheu Sim " +msg, Toast.LENGTH_SHORT).show();
        }else if(resultado == 2)
        {
            Toast.makeText(this, "Escolheu Não "+msg, Toast.LENGTH_SHORT).show();
        }else
        {
            Toast.makeText(this, "Não definido "+msg, Toast.LENGTH_SHORT).show();
        }

    }
  }

    }
}