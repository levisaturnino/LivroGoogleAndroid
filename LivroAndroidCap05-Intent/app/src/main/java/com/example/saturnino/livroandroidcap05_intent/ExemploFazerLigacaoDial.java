package com.example.saturnino.livroandroidcap05_intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by saturnino on 08/04/15.
 */
public class ExemploFazerLigacaoDial extends ActionBarActivity implements View.OnClickListener {

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_ligacao);

final Button button = (Button) findViewById(R.id.botaoOK);
        button.setOnClickListener(this);
        }
@Override
public void onClick(View v) {
        EditText campoEndereco = (EditText) findViewById(R.id.campoEndereco);
        String endereco = campoEndereco.getText().toString();

        Uri uri = Uri.parse("tel:" + endereco);

        Intent intent = new Intent(Intent.ACTION_DIAL,uri);

        startActivity(intent);

        }
}