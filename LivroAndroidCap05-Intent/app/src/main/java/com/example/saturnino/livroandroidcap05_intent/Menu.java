package com.example.saturnino.livroandroidcap05_intent;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by saturnino on 06/04/15.
 */
public class Menu extends ListActivity {

    private static final String[] nomes = new String[]{
            "Exemplo Abrir Browser","Exemplo Ligação Call","Exemplo Ligação Dial",
            "Exemplo Contato Celular","Exemplo Todos os Contatos",
            "Exemplo StartActivityForResult","Exemplo Detalhe do Contato","Exemplo Contato pelo Nome","Exemplo StartActivityForResult 2","Sair"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    this.setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,nomes));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position)
        {
            case 0:
                startActivity(new Intent(this,ExemploAbrirBrowser.class));
                break;
            case 1:
                startActivity(new Intent(this,ExemploFazerLigacaoCall.class));
                break;
            case 2:
                startActivity(new Intent(this,ExemploFazerLigacaoDial.class));
                break;
            case 3:
                startActivity(new Intent(this,ExemploContatoCelular.class));
                break;
            case 4:
                startActivity(new Intent(this,ExemploContatoTodosCelular.class));
                break;
            case 5:
                startActivity(new Intent(this,ExemploStartActivityForResult.class));
                break;
            case 6:
                startActivity(new Intent(this,ExemploDetalheContato.class));
                break;
            case 7:
                startActivity(new Intent(this,ExemploContatoPeloNome.class));
                break;
            case 8:
                startActivity(new Intent(this,ExemploStartActivityForResult2.class));
                break;
            default:
                finish();
        }
    }
}
