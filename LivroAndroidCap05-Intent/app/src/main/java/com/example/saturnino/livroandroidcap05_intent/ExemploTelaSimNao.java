package com.example.saturnino.livroandroidcap05_intent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;


public class ExemploTelaSimNao extends ActionBarActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_sim_nao);

        final Button sim = (Button) findViewById(R.id.btsim);
        final Button não = (Button) findViewById(R.id.btnao);

        sim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Cria a Intent
                Intent it = new Intent();
                // Seta a mensagem de retorno
                it.putExtra("msg", "Clicou em SIm !");
                // Seta o status do resultado e a Intent
                setResult(1, it);
                finish();
            }
        });
        não.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Cria a Intent
                Intent it = new Intent();
                // Seta a mensagem de retorno
                it.putExtra("msg", "Clicou em SIm !");
                // Seta o status do resultado e a Intent
                setResult(2, it);
                finish();
            }
        });

    }

}
